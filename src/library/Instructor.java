package library;

public class Instructor {
	String name;
	String idDepartment;
	public Instructor(String name,String idDepartment){
		this.name = name;
		this.idDepartment = idDepartment;
	}
	public void setname(String n){
		name=n;
	}
	
	public String getName(){
		return "Instructor Name: "+name;
	}
	
	public void setID_Department(String num){
		idDepartment=num;
	}
	
	public String getID_Department(){
		return "idDepartment: "+idDepartment;
	}
}
